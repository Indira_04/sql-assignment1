CREATE TABLE Producers (Id int identity primary key,Name varchar(100) ,Company varchar(100),CompanyEstDate date)
CREATE TABLE Actors (Id int identity primary key,Name varchar(100) ,Gender varchar(10),DOB date)
CREATE TABLE Movies (Id int identity primary key,Name varchar(100) ,Language varchar(10),ProducerId int references Producers(Id),Profit decimal(18,6))
CREATE TABLE Genre(Id int identity primary key,Name varchar(20));
CREATE TABLE MovieGenreMapping(Id int identity primary key, MovieId int references Movies(id),GenreId int references Genre(id));
CREATE TABLE MovieActorMapping (Id int identity primary key,MovieId int references Movies(id),ActorsId int references Actors(id))

