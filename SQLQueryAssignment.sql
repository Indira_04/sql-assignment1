USE IMDBTest

--Update Profit of all the movies by +1000 where producer name contains 'run'

UPDATE Movies
SET  Movies.Profit+=1000
from Movies A INNER JOIN Producers B
ON A.Producerid =B.id 
WHERE B.Name LIKE '%run%'


--List down actors details which have acted in movies where producer name ends with 'n'

SELECT  dISTINCT A.Id , A.Name , A.Gender , A.DOB  FROM Movies M INNER JOIN Producers P
ON M.Producerid = P.id
AND P.Name LIKE '%n'
INNER JOIN MovieActorMapping mp ON M.id= mp.movieId
INNER JOIN Actors A ON A.id=mp.ActorsId


--Find the avg age of male and female actors that were part of a movie called 'Terminal'  --Should return two rows 

SELECT  A.Gender,AVG(DATEDIFF(YY,A.DOB, GETDATE())) As 'Average Age'  FROM Movies M 
INNER JOIN MovieActorMapping MP ON M.Id=MP.MovieId AND M.Name= 'Terminal'
INNER JOIN Actors A ON MP.ActorsId= A.Id GROUP BY (A.Gender)


--Find the third oldest female actor

SELECT   (DATEDIFF(YY,A.DOB, GETDATE())) As 'Age'  ,A.Name
FROM Actors A WHERE a.Gender='Female' ORDER BY A.DOB ASC 
OFFSET  3-1 ROWS FETCH NEXT 1 ROWS ONLY

--List down top 3 profitable movies 

SELECT TOP 3 * FROM Movies ORDER BY Profit DESC 

--List down the oldest actor and Movie Name for each movie
/*SELECT A.Name,M.Name  FROM  MovieActorMapping Mp INNER JOIN Movies M
ON Mp.MovieId = M.Id
INNER JOIN Actors A ON A.Id = Mp.ActorsId 
GROUP BY M.Name,A.Name ,M.Id
ORDER BY M.Id*/
Select A2.Name,M1.Name FROM Actors A2 INNER JOIN MovieActorMapping Mp2 ON Mp2.ActorsId=A2.Id
INNER JOIN Movies M1 ON Mp2.MovieId=M1.Id
WHERE A2.DOB <= (select MIN(A3.DOB) FROM Actors A3 INNER JOIN MovieActorMapping Mp3 ON A3.Id=Mp3.ActorsId WHERE Mp3.MovieId=Mp2.MovieId)


--Find duplicate movies having the same name and their count
SELECT M.Name , count(*) AS 'COUNT' FROM Movies M
GROUP BY M.Name 
HAVING COUNT(*)>=2
--List down all the producers and +the movie name(even if they dont have a movie)

SELECT P.Name AS 'PRODUCER NAME' ,ISNULL(M.Name,'No Movie') AS 'MOVIE NAME' FROM Producers P 
LEFT OUTER JOIN Movies M  ON P.Id=M.ProducerId


--After all the queries are done
--Normalise it (If not normalised)
